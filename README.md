# EVE Events

- [EVE Events Plain ICS (right-click to copy link to add manually)](https://git.ufz.de/it-events/eve/-/raw/main/eve.ics)
- [EVE Events with webcal:// (just left-click it)](webcal://git.ufz.de/it-events/eve/-/raw/main/eve.ics)
